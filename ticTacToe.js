export class TicTacToe {
	constructor() {
		this.CURRENTPLAYER = 1;
		this.MOVES = [];
		this.BOARD = [0, 0, 0, 0, 0, 0, 0, 0, 0];
  	}

  	moves() {
  		let result = [];
  		if(this.winner()>0) {
  			return result;
  		}
		for(let i=0; i<this.BOARD.length; i++) {
			if(this.BOARD[i] == 0) {
				result.push(i);
			}
		}
		return result;
  	}

  	move(move) {
  		this.BOARD[move] = this.CURRENTPLAYER;
		this.MOVES.push(move);
  		this.CURRENTPLAYER = 3 - this.CURRENTPLAYER;
  	}

  	undo() {
//  		this.BOARD[this.BOARD.length-1] = 0;
  		let move = this.MOVES.pop();
  		this.BOARD[move] = 0;
  		this.CURRENTPLAYER = 3 - this.CURRENTPLAYER;
  	}

  	board() {
  		return this.BOARD;
  	}

  	winner() {
  		//Check rows
  		if(this.BOARD[0] == this.BOARD[1] && this.BOARD[1] == this.BOARD[2] && this.BOARD[0] != 0) {
  			return this.BOARD[0];
  		}
  		if(this.BOARD[3] == this.BOARD[4] && this.BOARD[4] == this.BOARD[5] && this.BOARD[3] != 0) {
  			return this.BOARD[3];
  		}
  		if(this.BOARD[6] == this.BOARD[7] && this.BOARD[7] == this.BOARD[8] && this.BOARD[6] != 0) {
  			return this.BOARD[6];
  		}

  		//Check columns
  		if(this.BOARD[0] == this.BOARD[3] && this.BOARD[3] == this.BOARD[6] && this.BOARD[0] != 0) {
  			return this.BOARD[0];
  		}
  		if(this.BOARD[1] == this.BOARD[4] && this.BOARD[4] == this.BOARD[7] && this.BOARD[1] != 0) {
  			return this.BOARD[1];
  		}
  		if(this.BOARD[2] == this.BOARD[5] && this.BOARD[5] == this.BOARD[8] && this.BOARD[2] != 0) {
  			return this.BOARD[2];
  		}

  		//Check cross
  		if(this.BOARD[0] == this.BOARD[4] && this.BOARD[4] == this.BOARD[8] && this.BOARD[0] != 0) {
  			return this.BOARD[0];
  		}
  		if(this.BOARD[6] == this.BOARD[4] && this.BOARD[4] == this.BOARD[2] && this.BOARD[6] != 0) {
  			return this.BOARD[6];
  		}
  		return 0;
  	}
}