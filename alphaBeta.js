var evaluateBoard = function (game, player)
{
  let winner = game.winner();
  if(winner==0) {
    return 0;
  }
  if(winner==player) {
    return 1000;
  }
  return -1000;
}

let forcestop = 20000;

var calcBestMove = function(depth, game, playerColor,
                            alpha=Number.NEGATIVE_INFINITY,
                            beta=Number.POSITIVE_INFINITY,
                            isMaximizingPlayer=true) {
  // Base case: evaluate board
  let value = 0;
  var bestMove = null; // best move not set yet
  var possibleMoves = game.moves();
  if(depth==3) {
//    console.log(game, possibleMoves);
  }
  if(forcestop-- < 0) {
    return;
  }
  if(depth === 0 || possibleMoves.length == 0) {
    value = evaluateBoard(game, playerColor);
    return [value, null];
  }
  // Recursive case: search possible moves
  // Set random order for possible moves
//  possibleMoves.sort(function(a, b){return 0.5 - Math.random()});
  // Set a default best move value
  var bestMoveValue = isMaximizingPlayer ? Number.NEGATIVE_INFINITY
                                         : Number.POSITIVE_INFINITY;
  // Search through all possible moves
  for (var i = 0; i < possibleMoves.length; i++) {
    var move = possibleMoves[i];
    // Make the move, but undo before exiting loop
    game.move(move);
    // Recursively get the value from this move
    value = calcBestMove(depth-1, game, playerColor, alpha, beta, !isMaximizingPlayer)[0];
    // Log the value of this move
    console.log(isMaximizingPlayer ? 'Max: ' : 'Min: ', depth, move, value,
                bestMove, bestMoveValue, forcestop);

    if (isMaximizingPlayer) {
      // Look for moves that maximize position
      if (value > bestMoveValue) {
        bestMoveValue = value;
        bestMove = move;
      }
      alpha = Math.max(alpha, value);
    } else {
      // Look for moves that minimize position
      if (value < bestMoveValue) {
        bestMoveValue = value;
        bestMove = move;
      }
      beta = Math.min(beta, value);
    }
    // Undo previous move
    game.undo();
    // Check for alpha beta pruning
    if (beta <= alpha) {
//      console.log('Prune', alpha, beta);
      break;
    }
  }
//  console.log("best move: ", bestMove, " ", bestMoveValue);
  return [bestMoveValue, bestMove];
}

import {TicTacToe} from './ticTacToe.js'; 

let game = new TicTacToe();
//game.move(0);
//game.move(8);
//game.move(2);
//game.move(1);
console.log(game);
let test = calcBestMove(9, game, 1);
console.log(test);
